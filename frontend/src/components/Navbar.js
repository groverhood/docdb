import React from 'react';
import { NavLink } from 'react-router-dom';
import logo from './../icon.png';
import './navbar_styles.css';

function Navbar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-custom">
            <a className="navbar-brand" href="/">
                <img src={logo} className="logo-image" alt='logo' />
                MyMedtro
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#minnavbar"
                aria-controls="minnavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="minnavbar">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/doctors">
                            Doctors
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/transit">
                            Transit
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/aux_services">
                            Auxiliary Services
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/visualizations">
                            Visualizations
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/provider_visualizations">
                            Provider Visualizations
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" activeClassName="nav-link active" to="/about">
                            About
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Navbar