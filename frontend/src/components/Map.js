import React from 'react';
import { Map } from 'google-maps-react';
import { GoogleApiWrapper } from 'google-maps-react';


export class MapContainer extends React.Component {
  render() {
    return <Map
      style={{ height: '300px', width: '500px' }}
      google={this.props.google}
      zoom={14}
      initialCenter={{
        lat: this.props.lat,
        lng: this.props.long
      }} />
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyCvhJ47L9w8HreiXrkByPzIf_ulr5F7JIs'
})(MapContainer)

