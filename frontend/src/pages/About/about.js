import React from 'react'
import { Container, CardGroup, Row, Card } from 'react-bootstrap'
import { members, tools, apis } from './about_info'
import TeamCard from './team_card'
import './../Styles/about_styles.css'
import gitlabImg from './Images/gitlab.svg'
import postmanImg from './Images/postman.png'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ShareIcon from '@material-ui/icons/Share';
import ToolCard from './tool_card'
import APICard from './api_card'
import Loading from './../../components/loading.js'

class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info: null,
            loading: true
        }
    }
    componentDidMount() {
        fetch('/api/about')
            .then(res => res.json()).then(data => { this.setState({ info: data, loading: false }) })
    }

    render() {
        const info = this.state.info
        let totalTests = 0

        members.map((member) => (member.commits = (info ? info['contributors'][member.name]['commits'] : 0)))
        members.map((member) => (member.issues = (info ? info['contributors'][member.name]['issues'] : 0)))
        members.map((member) => (totalTests += member.tests))


        return (
            <Container>
                {/* description */}
                <h1>About</h1>
                <br />
                <h4> MyMedtro is a website to help users find affordable and healthcare-compatible doctors as well as a safe and convenient transportation route to their offices.</h4>
                <br />

                {/* team cards */}
                <h1>Team</h1>
                { this.state.loading ? <Loading/> : (
                <CardGroup>
                    <Row className="justify-content-md-center">
                        {members.map((member) => (<a className="Link" key={member.name} href={member.linkedin}><TeamCard key={member.name} member={member} /></a>))}
                    </Row>
                </CardGroup> )}
                <br />

                {/* repo stats */}
                <h1>Repository Statistics</h1>
                <br />
                <div className='statBox' style={{ color: 'black' }}>
                    <div className='statItem'>Commits <ShareIcon /> {info ? info['totalCommits'] : 0}</div>
                    <div className='statItem'> Issues <ListAltIcon /> {info ? info['totalIssues'] : 0}</div>
                    <div className='statItem'> Tests <CheckCircleOutlineIcon /> {totalTests}</div>
                </div>
                <br />

                {/* toocards */}
                <h1>Tools</h1>
                <CardGroup>
                    <Row className="justify-content-md-center">
                        {tools.map((tool, idx) =>
                            <ToolCard key={idx} tool={tool} />)
                        }
                    </Row>
                </CardGroup>
                <br />

                {/* api cards */}
                <h1>APIs Used</h1>
                <CardGroup>
                    <Row className="col d-flex justify-content-center">
                        {apis.map((api, idx) => (
                            <APICard key={idx} api={api} /> )
                            )
                        }
                    </Row>
                </CardGroup>
                <br />

                {/* repo and postman links */}
                <h1>Gitlab Repo and Postman API</h1>
                <br />
                <Container >
                    <CardGroup>
                        <Row className="col d-flex justify-content-center">
                            <a href='https://gitlab.com/groverhood/docdb'>
                                <Card className='icons'>
                                    <Container className="toolPicContainer">
                                        <Card.Img variant="top" src={gitlabImg} className="toolpic" />
                                    </Container>
                                </Card>
                            </a>
                            <a href='https://documenter.getpostman.com/view/12964716/TVmFif5q'>
                                <Card className='icons'>
                                    <Container className="toolPicContainer">
                                        <Card.Img variant="top" src={postmanImg} className="toolpic" />
                                    </Container>
                                </Card>
                            </a>
                        </Row>
                    </CardGroup>
                </Container>
                <br></br><br />
            </Container>
        )
    }
}

export default About