
import { formatProducts, formatAddress } from './aux_utils'

describe('aux_utils.js test suite', () => {
    it('tests formatProducts()', () => {
        expect(formatProducts('[]')).toStrictEqual([])
        expect(formatProducts('[""]')).toStrictEqual([""])
        expect(formatProducts('["hello", "world"]')).toStrictEqual(["hello", "world"])
    })
    it('tests formatAddress()', () => {
        expect(formatAddress({ 
            address: '241 Qux St.',
            city: 'Fooville',
            state: 'State Bar',
            zipcode: '58541'
        })).toStrictEqual('241 Qux St.\nFooville, State Bar 58541')
    })
})