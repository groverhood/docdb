
import React from 'react'
import AuxInstance from './aux_instance'

import { render, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('AuxInstance testing suite', () => {
    configure({ adapter: new Adapter() })
    let auxInstance = render(<AuxInstance />)
    it('checks for correct properties', () => {
        expect(auxInstance.find('.Instance').length).toBe(1)
    })
})