import React from 'react'
import { Container, CardGroup, Row } from 'react-bootstrap'
import DoctorCard from '../Doctors/doctor_card'
import RouteCard from './route_card'
import AuxCard from './../AuxServices/aux_card.js'

class TransitInstance extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            startImg: null,
            endImg: null,
            doc_npi: null,
            aux_id: null,
        }
    }

    componentDidMount() {
        const aux_loc = this.props.location.state.origin[0].aux_location[0]
        const doc_loc = this.props.location.state.destination[0].doc_location[0]
        const startAddy = aux_loc.address
        const addy = doc_loc.address

        fetch(`https://maps.googleapis.com/maps/api/streetview?size=300x300&location=${addy}&key=AIzaSyA2WpzkLDlb7eLIRQ29imgybOIJ9Vr6zlE`)
            .then(res => this.setState({ endImg: res["url"] }))

        fetch(`https://maps.googleapis.com/maps/api/streetview?size=300x300&location=${startAddy}&key=AIzaSyA2WpzkLDlb7eLIRQ29imgybOIJ9Vr6zlE`)
            .then(res => this.setState({ startImg: res["url"] }))

        const doc_npi = this.props.location.state.destination[0].npi_number
        this.setState({ doc_npi: doc_npi })

        const aux_id = this.props.location.state.origin[0].id
        this.setState({ aux_id: aux_id })
    }

    render() {
        const sections = this.props.location.state.route ? this.props.location.state.route[0].sections : null
        return (
            <Container className='fluid'>
                <h1>{this.props.location.state.origin[0].company_name} to {this.props.location.state.destination[0].full_name}</h1>
                <br />

                {sections ? (<CardGroup>
                    <Row className="justify-content-md-center">
                        {sections.map((sect, idx) => <RouteCard key={idx} idx={idx} section={sect} last={idx === sections.length - 1} />)}
                    </Row>
                </CardGroup>) : <h2>No routing information at this time</h2>}
                <br></br>

                <div className='row' style={{justifyContent: 'center'}}>
                    <div className='col'>
                        <h4>Start</h4>
                        <img src={this.state.startImg} alt={'starting location'} style={{ margin: '10px' }} />
                    </div>
                    <div className='col'>
                        <h4>End</h4>
                        <img src={this.state.endImg} alt={'ending location'} style={{ margin: '10px' }} />
                    </div>
                </div>
                <br/><br/>
                
                <h4>Related Doctor and Pharmacy</h4>
                <div className='row' style={{justifyContent: 'center'}}>
                    <CardGroup>
                        {this.state.doc_npi ?
                            <DoctorCard id={this.state.doc_npi} /> : <div>loading</div>}
                        {this.state.aux_id ?
                            <AuxCard id={this.state.aux_id} /> : <div>loading</div>}
                    </CardGroup>
                </div>
                <br/><br/>
            </Container>)
    }

}

export default TransitInstance