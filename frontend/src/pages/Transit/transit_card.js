import React from 'react'
import Card from 'react-bootstrap/Card'
import './../Styles/mod_styles.css'
import Highlighter from "react-highlight-words";
import { Redirect } from 'react-router-dom'
import { searchClient } from './../../components/SearchClient.js'
import {formatTime, getElapsedTime} from './transit_utils.js'
import Loading from './../../components/loading.js'

const index = searchClient.initIndex('transit_connections')

class TransitCard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            transit: [],
            redirect: false,
            route: null,
            loading: true
        }
    }

    componentDidMount() {
        index.search('', { facetFilters: [`id:${this.props.id}`] })
            .then(result => this.getRoute(result.hits[0]))
    }

    getRoute(res) {
        // grab the coordinates
        const originLat = res.aux_latitude
        const originLong = res.aux_longitude
        const destLat = res.doctor_latitude
        const destLong = res.doctor_longitude

        // get realtime transit data
        const req_str = 'https://transit.hereapi.com/v8/routes?origin=' + originLat + ',' + originLong + '&mode=pedestrian&apiKey=fEG1fE4oIMTMG6uBiM7gQH7_yACCQ36e60HsCObc7S0&destination=' + destLat + ',' + destLong
        fetch(req_str)
            .then(res => res.json())
            .then(data => Object.keys(data).includes('notices') ? this.setState({ transit: res, route: null, loading: false }) : this.setState({ transit: res, route: data.routes, loading: false }))
    }

    // helps with routing
    onCardClick() {
        this.setState({ redirect: true })
    }

    render() {
        const origin = this.state.transit.aux_company_name ? this.state.transit.aux_company_name : ''
        const destination = this.state.transit.doctor_name ? this.state.transit.doctor_name : ''
        const id = this.state.transit.id ? this.state.transit.id : ''
        const city = this.state.transit.aux_city ? this.state.transit.aux_city : ''
        
        const sections = this.state.route ? this.state.route[0] ? this.state.route[0].sections : [] : []
        const departure = sections ? (sections[0] ? sections[0].departure : null) : null
        const dep_time = departure ? departure.time : null
        const arrival = sections ? sections.length ? (sections[sections.length - 1] ? sections[sections.length - 1].arrival : null) : null : null
        const ar_time = arrival ? arrival.time : null
        const total = dep_time ? ar_time ? getElapsedTime(ar_time, dep_time) : '-' : '-'

        let modes = []
        sections.map(section => modes.push(section.type))
        modes = Array.from(new Set(modes))
        // redirect only if the state is true
        if (this.state.redirect) {
            return (
                <Redirect to={{
                    pathname: `/transit/${this.props.id}`,
                    state: { route: this.state.route, origin: this.state.transit.aux_service, destination: this.state.transit.doctor }
                }} />
            )
        }
        return (
            this.state.loading ? <Card className="InstanceCard"><br/><br/><Loading/></Card> :
            <Card onClick={this.onCardClick.bind(this)} className="InstanceCard" >
                <Card.Body>
                    <Card.Title>
                        <Highlighter searchWords={[this.props.search]} textToHighlight={origin + ' to ' + destination} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                    </Card.Title>
                    <Card.Text>
                        ID:
                        <Highlighter searchWords={[this.props.search]} textToHighlight={' ' + id.toString()} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                        <br />
                        City:
                        <Highlighter searchWords={[this.props.search]} textToHighlight={' ' + city} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                        <br/>
                        Departure: {' ' + formatTime(dep_time)} <br/>
                        Arrival: {' ' + formatTime(ar_time)} <br/>
                        Duration: {' ' + total} <br/>
                        Modes: {modes.join(', ')}
                    </Card.Text>
                </Card.Body>
            </Card>)
    }
}

export default TransitCard