# Models

## Doctors Table Schema: 

```python
__tablename__ = "doctors"
npi_number = Column(Integer, primary_key=True)
enumeration_type = Column(String)
full_name = Column(String)
name = Column(String)
gender = Column(String)
last_updated = Column(DateTime)
address = Column(String)
city = Column(String)
state = Column(String)
zipcode = Column(String)
phone_number = Column(String)
fax_number = Column(String)
specialities = Column(String)
doc_location = relationship("DoctorsLocationMap", back_populates="doctor")
```

## Doctors Location Mapping Table Schema: 

```python
__tablename__ = "doctors_location_map"
address = Column(String)
city = Column(String)
state = Column(String)
latitude = Column(String)
longitude = Column(String)
doctor_id = Column(Integer, ForeignKey("doctors.npi_number"), primary_key=True)
doctor = relationship("Doctors", back_populates="doc_location")
```

## Aux Services Table Schema: 

```python
__tablename__ = "aux_services"
id = Column(Integer, primary_key=True, autoincrement=True)
company_name = Column(String)
address = Column(String)
city = Column(String)
state = Column(String)
zipcode = Column(String)
phone = Column(String)
product = Column(String)
image = Column(String)
aux_location = relationship("AuxServicesLocationMap", back_populates="aux_service")
```

## Aux Services Location Mapping Schema:

```python
__tablename__ = "aux_services_location_map"
address = Column(String)
city = Column(String)
state = Column(String)
latitude = Column(String)
longitude = Column(String)
aux_service_id = Column(Integer, ForeignKey("aux_services.id"), primary_key=True)
aux_service = relationship("AuxServices", back_populates="aux_location")
```

## Transit Connections Table Schema:

```python 
__tablename__ = "transit_connections"
id = Column(Integer, primary_key=True, autoincrement=True)
aux_service_id = Column(Integer)
doctor_id = Column(Integer)
aux_latitude = Column(String)
aux_longitude = Column(String)
aux_city = Column(String)
doctor_latitude = Column(String)
doctor_longitude = Column(String)
doctor_city = Column(String)
```

