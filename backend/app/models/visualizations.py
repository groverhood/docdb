from .. import db
import requests
from datetime import datetime


def present(resultproxy):
    return [
        {column.title(): value for column, value in rowproxy.items()}
        for rowproxy in resultproxy
    ]


def count_doctors_by_city_and_gender():
    query = """
		select City, 
		count(case when gender='F' then 1 end) as Female, 
		count(case when gender='M' then 1 end) as Male 
		from public.doctors group by city
	"""
    # execute returns a resultproxy obj
    resultproxy = db.engine.execute(query)
    # present it as a list
    return present(resultproxy)


def count_cvs_by_city():
    query = """
		select City, 
		count(case when company_name like 'Cvs%%' then 1 end) as Count 
		from public.aux_services group by city
	"""
    # execute returns a resultproxy obj
    resultproxy = db.engine.execute(query)
    # present it as a list
    return present(resultproxy)


def count_doctor_specialities():
    query = """
		select
		(select count(*) from public.doctors where specialities like '%%Surgery%%') as Surgery,
		(select count(*) from public.doctors where specialities like '%%Internal Medicine%%') as Internal_Medicine,
		(select count(*) from public.doctors where specialities like '%%Pediatric%%') as Pediatric,
		(select count(*) from public.doctors where specialities like '%%Family%%') as Family_Practice,
		(select count(*) from public.doctors where specialities like '%%Dentist%%') as Dentist,
		(select count(*) from public.doctors where specialities like '%%Radiology%%') as Radiology,
		(select count(*) from public.doctors where specialities like '%%Physician Assistant%%') as Assistant,
		(select count(*) from public.doctors where specialities like '%%Nurse%%') as Nurse,
		(select count(*) from public.doctors where specialities like '%%Social%%') as Social_Worker,
		(select count(*) from public.doctors where specialities like '%%Neuro%%') as Neuro,
		(select count(*) from public.doctors where specialities like '%%Anesthe%%') as Anesthesiologist
	"""
    # execute returns a resultproxy obj
    resultproxy = db.engine.execute(query)
    
    presented = present(resultproxy)[0]
    
    # present it as a list
    return [{'Speciality': speciality, 'Count': count} for speciality, count in presented.items()]
    


"""
Provider Visualizations
"""


def get_politician_counts(*looking_for):
    # helper function
    counts = {}

    page = 1
    while True:
        response = requests.request(
            "GET", "https://api.texasvotes.me/politician?page={}".format(page)
        )
        # break when reach end
        if response.status_code == 404:
            break

        response = response.json()

        # get list of dicts
        politicians = response["page"]

        # loop and count the value we are looking for
        for politician in politicians:

            # look for the right value in the json
            value = ""
            for item in looking_for:
                if item in politician:
                    value = politician[item]
                    break

            # if value not found then move onto next one
            if value == "":
                continue

            # update counts
            if value in counts:
                counts[value] += 1
            else:
                counts[value] = 1

        # update page
        page += 1

    return counts


def get_party_counts_by_politicians():
    """
    Count of politicians that ran per party
    """

    # party letter : count
    to_convert = get_politician_counts("party")

    # build right format
    return [{'Party': party, 'Count': count} for party, count in to_convert.items()]



def get_running_for_counts():
    """
    counts of offices that were ran for
    """

    # running_for : count
    to_convert = get_politician_counts("current", "running_for")

    # build right format
    ret = []
    for office, count in to_convert.items():
        fixed_office = office.split('_')
        fixed_office[0], fixed_office[-1] = fixed_office[0].upper(), fixed_office[-1].title()
        ret.append({'Office': ' '.join(fixed_office), 'Count': count})

    return ret


def get_year(datetime_str):
    # sample: 2020-02-28T06:00:00.000Z
    date = datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")

    return date.year


def get_election_counts(party=False, total_voter_count=False):
    counts = {}

    page = 1
    while True:
        response = requests.request(
            "GET", "https://api.texasvotes.me/election?page={}".format(page)
        )
        # break when reach end
        if response.status_code == 404:
            break

        response = response.json()

        # get list of dicts
        elections = response["page"]

        # loop and count parties/total voters
        for election in elections:

            # if there are no results then skip
            if "results" not in election:
                continue

            # get the year
            year = get_year(election["dates"]["election_day"])

            if party:
                # count party winners
                party = election["results"]["winner"]["party"]

                # update party winners by year
                if year in counts:
                    sub_dict = counts[year]
                    if party in sub_dict:
                        sub_dict[party] += 1
                    else:
                        sub_dict[party] = 1
                else:
                    counts[year] = {party: 1}

            elif total_voter_count:
                # count total voters
                total_voters = election["results"]["total_voters"]

                # update total voters
                if year in counts:
                    counts[year] += total_voters
                else:
                    counts[year] = total_voters

        # update page
        page += 1

    return counts


def get_party_win_counts_by_year():
    """
    Count Party winners by year
    """
    to_convert = get_election_counts(party=True)

    ret = []
    for year, counts in to_convert.items():
        add_ = {}
        add_['Year'] = year
        for party, count in counts.items():
            add_[party] = count

        ret.append(add_)

    return sorted(ret, key=lambda item : item['Year'])





def get_total_voters_by_year():
    """
    Count of total voters by year
    """
    to_convert = get_election_counts(total_voter_count=True)

    return sorted([{'Year': year, 'Voters': voters / 1_000_000} for year, voters in to_convert.items()], key= lambda item: item['Year'])


def get_party_count_by_district():
    counts = {}

    page = 1
    while True:
        response = requests.request(
            "GET", "https://api.texasvotes.me/district?page={}".format(page)
        )
        # break when reach end
        if response.status_code == 404:
            break

        response = response.json()

        # get list of dicts
        districts = response["page"]

        # loop and count parties
        for district in districts:

            # if there are no results then skip
            if "party" not in district:
                continue

            party = district["party"]

            if party in counts:
                counts[party] += 1
            else:
                counts[party] = 1

        # update page
        page += 1

    return [{'Party': party, 'Count': count} for party, count in counts.items()]
