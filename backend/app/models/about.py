import requests


class Contributor:
    # Contributor class
    def __init__(self, name, *other_names):
        self.name = name
        self.all_names = set(other_names)
        self.all_names.add(name)
        self.commits = 0
        self.issues = 0

    def add_commits(self, value):
        self.commits += value

    def add_issue(self):
        self.issues += 1

    def get_dict(self):
        return {self.name: {"commits": self.commits, "issues": self.issues}}


class Contributors:
    # All Contributors class
    def __init__(self):
        self.all_contributors = []
        self.all_names = set()
        self.user_issues = 0
        self.total_commits = 0
        self.total_issues = 0

    def add_contributor(self, name, *other_names):
        self.all_contributors.append(Contributor(name, *other_names))
        # add all the names
        self.all_names.add(name)
        for other_name in other_names:
            self.all_names.add(other_name)

    def add_commits(self, name, value):
        for contributor in self.all_contributors:
            if name in contributor.all_names:
                contributor.add_commits(value)
                self.total_commits += value

    def add_issues(self, name):
        self.total_issues += 1
        # add to user issues count
        if name not in self.all_names:
            self.user_issues += 1
            return

        # add contributor issues
        for contributor in self.all_contributors:
            if name in contributor.all_names:
                contributor.add_issue()

    def __str__(self):
        # str method for debugging
        ret = [str(contributor.get_dict()) for contributor in self.all_contributors]
        ret.append(str({"Users": {"commits": 0, "issues": self.user_issues}}))
        ret.append("User_issues " + str(self.user_issues))
        ret.append("Total_commits " + str(self.total_commits))
        ret.append("Total_issues " + str(self.total_issues))
        return "\n".join(ret)

    def get_dict(self):
        # create the contribution dict
        contributors_dict = {}
        for contributor in self.all_contributors:
            contributors_dict.update(contributor.get_dict())
        # add the customers
        contributors_dict.update({"Users": {"commits": 0, "issues": self.user_issues}})
        # return the formatted response
        return {
            "contributors": contributors_dict,
            "totalIssues": self.total_issues,
            "totalCommits": self.total_commits,
        }


# backend code to get GitLab stats info
def get_about_info():
    # get all the contributors
    my_contributors = Contributors()
    my_contributors.add_contributor("Mahaa Noorani")
    my_contributors.add_contributor("Duncan Huntsinger", "Ubuntu")
    my_contributors.add_contributor("Divya Manohar")
    my_contributors.add_contributor("Saad Ahmad")
    my_contributors.add_contributor("Kyle Wang", "Kyle L Wang")

    # makes API call to get number of commits per team member
    response = requests.get(
        "https://gitlab.com/api/v4/projects/21350761/repository/contributors"
    ).json()

    # loop thru response
    for person in response:
        my_contributors.add_commits(person["name"], person["commits"])

    page = 1
    # handles GitLab issues pagination
    while True:
        # make API call to get GitLab issues info per team member
        response = requests.get(
            "https://gitlab.com/api/v4/projects/21350761/issues?" + f"&page={page}"
        ).json()

        if len(response) == 0:
            break

        # add issue count for each team member
        for issue in response:
            name = issue["author"]["name"]
            my_contributors.add_issues(name)
        page += 1

    return my_contributors.get_dict()


if __name__ == "__main__":
    x = get_about_info()
    print(x)
