from algoliasearch.search_client import SearchClient
from .algolia_helper import (
    get_aux_service,
    get_doctor,
    clean_doctor,
    TransitConnections,
)
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def fill_transit_index(
    app_id,
    api_key,
    endpoint,
    settings,
    update_settings=False,
    drop_index=False,
    fill=False,
):
    # create the client
    client = SearchClient.create(app_id, api_key)
    # create index run this once
    index = client.init_index("transit_connections")

    # drop table
    if drop_index:
        print("Dropping Index")
        index.delete()

    if fill:
        # get transit info
        all_connections = []

        try:
            response = requests.request("GET", endpoint + "transit_connections")
        except:
            print("Endpoint Incorrect, fix endpoint in algolia.json")
            return

        print("Getting all Transit Connections, this will take some time")
        # since this is paginated
        while True:
            # deal with the first response and then work with the rest
            response = response.json()
            batch_connections = response["objects"]

            # looping thru the list and adding the connections to all connects
            for connection in batch_connections:

                # set objectID
                connection["objectID"] = int(connection["id"])

                # adding doctor
                doc_response = get_doctor(connection["doctor_id"], endpoint)
                connection["doctor"] = [doc_response]

                # add doctor name
                connection["doctor_name"] = doc_response["full_name"]

                # adding aux_response
                aux_response = get_aux_service(connection["aux_service_id"], endpoint)
                connection["aux_service"] = [aux_response]

                # add company name
                connection["aux_company_name"] = aux_response["company_name"]

                all_connections.append(connection)

            if response["page"] == response["total_pages"]:
                break

            response = requests.request(
                "GET",
                endpoint + "transit_connections?page={}".format(response["page"] + 1),
            )

        # doing this before adding so if there is a failure above the index is still there
        print("Dropping Index before adding new transits")
        index.delete()

        # save all docs to index
        print("Saving new transits to index")
        index.save_objects(all_connections, {"autoGenerateObjectIDIfNotExist": False})

    # update settings
    if fill or update_settings:
        print("Setting Index settings for Transit Index")
        index.set_settings(settings)


def fill_doc_index(
    login,
    app_id,
    api_key,
    endpoint,
    settings,
    update_settings=False,
    drop_index=False,
    fill=False,
):

    # create engine and session for Postgres
    engine = create_engine(login)
    Session = sessionmaker(bind=engine)

    # create the client
    client = SearchClient.create(app_id, api_key)
    # create index run this once
    index = client.init_index("doctors")

    # drop table
    if drop_index:
        print("Dropping Index")
        index.delete()

    if fill:
        # get docs from endpoint
        all_docs = []
        try:
            response = requests.request("GET", endpoint + "doctors")
        except:
            print("Endpoint Incorrect, fix endpoint in algolia.json")
            return

        # create session
        my_session = Session()

        print("Getting all Doctors, this will take some time")
        while True:
            # deal with the first response and then work with the rest
            response = response.json()
            batch_doctors = response["objects"]

            # looping thru the list and adding the connections to all connects
            for doctor in batch_doctors:

                # set objectID
                doctor["objectID"] = int(doctor["npi_number"])

                # pull transit connection row that links to the doctor npi
                trans_query = my_session.query(TransitConnections).filter(
                    TransitConnections.doctor_id == doctor["npi_number"]
                )
                trans_con = trans_query.first()
                trans_con = trans_con.__dict__
                trans_con.pop("_sa_instance_state", None)
                doctor["transit_connection"] = [trans_con]

                # pull the aux column from the transit connection row and pull the aux row
                aux_response = get_aux_service(trans_con["aux_service_id"], endpoint)
                doctor["aux_service"] = [aux_response]

                # clean doctor
                clean_doctor(doctor)

                all_docs.append(doctor)

            if response["page"] == response["total_pages"]:
                break

            response = requests.request(
                "GET", endpoint + "doctors?page={}".format(response["page"] + 1)
            )

        # doing this before adding so if there is a failure above the index is still there
        print("Dropping Index before adding new doctors")
        index.delete()

        # save all docs to index
        print("Saving new docs to index")
        index.save_objects(all_docs, {"autoGenerateObjectIDIfNotExist": False})

    # update settings
    if fill or update_settings:
        print("Setting Index settings for Doctors Index")
        index.set_settings(settings)


def fill_aux_index(
    login,
    app_id,
    api_key,
    endpoint,
    settings,
    update_settings=False,
    drop_index=False,
    fill=False,
):

    # create engine and session for Postgres
    engine = create_engine(login)
    Session = sessionmaker(bind=engine)

    # create the client
    client = SearchClient.create(app_id, api_key)
    # create index run this once
    index = client.init_index("aux_services")

    # drop table
    if drop_index:
        print("Dropping Index")
        index.delete()

    if fill:
        # get aux from endpoint
        all_aux_services = []
        try:
            response = requests.request("GET", endpoint + "aux_services")
        except:
            print("Endpoint Incorrect, fix endpoint in algolia.json")
            return

        # create session
        my_session = Session()

        print("Getting all Aux Services, this will take some time")
        while True:
            # deal with the first response and then work with the rest
            response = response.json()
            batch_aux_services = response["objects"]

            # looping thru the list and adding the connections to all connects
            for aux_service in batch_aux_services:

                # set objectID
                aux_service["objectID"] = int(aux_service["id"])

                # pull transit connection row that links to the doctor npi
                trans_query = my_session.query(TransitConnections).filter(
                    TransitConnections.aux_service_id == aux_service["id"]
                )
                trans_con = trans_query.first()
                trans_con = trans_con.__dict__
                trans_con.pop("_sa_instance_state", None)
                aux_service["transit_connection"] = [trans_con]

                # pull the aux column from the transit connection row and pull the aux row
                doc_response = get_doctor(trans_con["doctor_id"], endpoint)
                aux_service["doctor"] = [doc_response]

                all_aux_services.append(aux_service)

            if response["page"] == response["total_pages"]:
                break

            response = requests.request(
                "GET", endpoint + "aux_services?page={}".format(response["page"] + 1)
            )

        # doing this before adding so if there is a failure above the index is still there
        print("Dropping Index before adding new Aux Services")
        index.delete()

        # save all aux to index
        print("Saving new Aux Services to index")
        index.save_objects(all_aux_services, {"autoGenerateObjectIDIfNotExist": False})

    # update settings
    if fill or update_settings:
        print("Setting Index settings for Aux Services Index")
        index.set_settings(settings)
