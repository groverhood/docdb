"""
File to Parse Aux Services data
"""


# gets 30 instances of the auxiliary services data
def parse_aux_data(response, lim=30):
    aux_data = {}
    i = 0
    while len(aux_data.keys()) < lim:
        company_name = response.json()[i]["dba_name"]
        if not company_name in aux_data:
            aux_data[company_name] = [response.json()[i]]
        else:
            aux_data[company_name].append(response.json()[i])
        i += 1

    return aux_data


# puts data in a presentable format to persist to data base
def present(aux_data, aux_id):
    ret = {}
    for dba_name in aux_data.keys():
        data = aux_data[dba_name]

        for i in range(len(data)):
            entry = data[i]
            if i == 0:
                company_name = entry["company_name"]

                # deals with store number
                if "#" in dba_name:
                    url = dba_name.replace("#", "%23")
                else:
                    url = dba_name
                # gets all of the necessary information from the api response
                address = entry["address"]
                city = entry["city"]
                state = entry["state"]
                zip_code = entry["zip"]
                phone = entry["phone"]
                product = [entry["prod_ctgry_name"]]
                image = url

                # updates the dictionary to contain the database information
                ret[dba_name] = {
                    "id": aux_id,
                    "name": company_name,
                    "address": address,
                    "city": city,
                    "state": state,
                    "zip code": zip_code,
                    "phone": phone,
                    "product": product,
                    "image": image,
                }
                aux_id += 1

            else:
                prod = entry["prod_ctgry_name"]
                ret[dba_name]["product"].append(prod)

    return ret
