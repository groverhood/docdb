from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

# create app
app = Flask(
    __name__,
    static_folder="../../frontend/build/static",
    template_folder="../../frontend/build",
)
CORS(app)

"""
Configure SQL Alchemy
"""
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config.from_pyfile("config.cfg")
db = SQLAlchemy(app)

from . import views
