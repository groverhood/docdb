import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options
from time import sleep
import requests

options = Options()
options.headless = True
driver = webdriver.Firefox(options=options)


class Selenium(unittest.TestCase):
    

    def test1(self):
        driver.get('https://www.mymedtro.com')
        result = driver.find_element_by_css_selector('.App')
        self.assertNotEqual(result, None)
        
    def test2(self):
        driver.get('https://www.mymedtro.com')
        result = driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    def test3(self):
        driver.get('https://www.mymedtro.com/doctors/')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'InstanceCard')))
        result = driver.find_elements_by_class_name('InstanceCard')
        self.assertEqual(len(result), 10)
        
    def test4(self):
        driver.get('https://www.mymedtro.com/aux_services/')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'Link')))
        result = driver.find_elements_by_class_name('Link')
        self.assertEqual(len(result), 10)
        
    def test5(self):
        driver.get('https://www.mymedtro.com/about/')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'about_card')))
        result = driver.find_elements_by_class_name('about_card')
        self.assertEqual(len(result), 5)
        
    def test6(self):
        driver.get('https://www.mymedtro.com/')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'nav-item')))
        result = driver.find_elements_by_class_name('nav-item')
        self.assertEqual(len(result), 6)

    def test7(self):
        driver.get('https://www.mymedtro.com/')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'btn-responsive')))
        result = driver.find_elements_by_class_name('btn-responsive')
        self.assertEqual(len(result), 3)

    # def test8(self):
    #     driver.get('https://www.mymedtro.com/transit/')
    #     WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CSS_SELECTOR, 'tr')))
    #     result = driver.find_elements_by_css_selector('tr')
    #     self.assertEqual(len(result), 1)


if __name__ == '__main__':
    unittest.main()